"use strict";

var app = angular.module('comprafacil', [
    'comprafacil.controllers',
    'comprafacil.services'
]);

app.config(["$routeProvider", function ($routeProvider) {
  $routeProvider
    .when("/products/:productId", {controller: "ProductsCtrl", templateUrl: "partials/product.html"})
    .when("/categories/:categoryId/products", {controller: "ProductsInCategoryCtrl", templateUrl: "partials/products_list.html"})
    .when("/categories", {controller: "CategoriesCtrl", templateUrl: "partials/categories.html"})
    .when("/categories/:categoryId", {controller: "CategoriesCtrl", templateUrl: "partials/categories.html"})
    .otherwise({redirectTo: "/categories"});
}]);
