'use strict';

var ctrls = angular.module('comprafacil.controllers', []);


ctrls.controller('CategoriesCtrl',function ($scope, comprafacilServices, $routeParams){
	var id = $routeParams.categoryId;

	comprafacilServices.categories(id).then(function (data) {
		console.log(data.data);
		var categories = data.data.categories;
		angular.forEach(categories, function(cat){
			cat.nextLink = "#/categories/" + cat.categoryId;
			if(cat.totalProducts > 0)
				cat.nextLink += "/products";
		});

		$scope.categories = categories;
		
	});
});


ctrls.controller('ProductsInCategoryCtrl',function ($scope, comprafacilServices, $routeParams){
	var id = $routeParams.categoryId;

	comprafacilServices.productsInCategory(id).then(function (data) {
		console.log(data.data);
		
		var products = data.data.products;
		angular.forEach(products, function(product){
			product.imageUrl = product.productSKUs[0].skuImage;
		});
		
		$scope.products = products;
	});
});

ctrls.controller('ProductsCtrl',function ($scope, comprafacilServices, $routeParams){
	var id = $routeParams.productId;

	comprafacilServices.products(id).then(function (data) {
		console.log(data.data);
		
		var product = data.data;
		product.imageUrl = product.productSKUs[0].skuImage;
		$scope.product = product;
	});
});

