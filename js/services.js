'use strict';

var services = angular.module('comprafacil.services', []);

services.factory('comprafacilServices', function ($http) {
  
  function load (path, params) {
    params = params || "";
    if(params === "")
      return $http.get("http://qa.comprafacil.cloudretail.com.br/api/v1" + path);
    else
      return $http.get("http://qa.comprafacil.cloudretail.com.br/api/v1" + path,{params:params});
  }
  return {
    
    categories: function (categoryId) {
        categoryId = categoryId || "";
        return load("/categories/" + categoryId);
    },
    productsInCategory: function (categoryId) {
        return load("/categories/" + categoryId + "/products");
    },
    products: function (productId) {
        return load("/products/" + productId);
    }
    
  }
});
